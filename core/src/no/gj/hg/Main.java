package no.gj.hg;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import no.gj.hg.systems.*;
import no.gj.hg.systems.InputCSystem;
import no.gj.hg.systems.CSystem;
import no.gj.hg.systems.utils;

import java.util.ArrayList;
import java.util.List;

public class Main extends ApplicationAdapter {

	Physics physicsSystem;
	InputCSystem inputSystem;
	RenderingCSystem renderingSystem;
	CameraCSystem cameraCSystem;
	TileMapCSystem tileMapCSystem;
	MeltSystem meltSystem;
	TextboxCSystem textboxCSystem;
	GunSystem gunSystem;
	List<Entity> entityList = new ArrayList<Entity>();
	List<CSystem> systems = new ArrayList<CSystem>();
	Game game;

	TiledMap tiledMap;
	TiledMapRenderer tiledMapRenderer;

	Sound music;
	Sound hover;


	
	@Override
	public void create () {
		game = new Game();

		loadLevel("level1.tmx");

		music = Gdx.audio.newSound(Gdx.files.internal("music/crystalcave.mp3"));
		hover = Gdx.audio.newSound(Gdx.files.internal("sound/hoveridle.mp3"));
		Sound meltSound = Gdx.audio.newSound(Gdx.files.internal("sound/meltsound.mp3"));
		Sound gunSound = Gdx.audio.newSound(Gdx.files.internal("sound/raygun1.mp3"));

		Sound hoverBooster = Gdx.audio.newSound(Gdx.files.internal("sound/hoverboost.mp3"));;


		physicsSystem = new Physics();
		inputSystem = new InputCSystem(hoverBooster, meltSound, gunSound);
		renderingSystem = new RenderingCSystem();
		cameraCSystem = new CameraCSystem();
		tileMapCSystem = new TileMapCSystem(tiledMap);
		meltSystem = new MeltSystem();
		textboxCSystem = new TextboxCSystem();
		gunSystem = new GunSystem();

		// note, order is very important.
		systems.add(cameraCSystem);
		systems.add(tileMapCSystem);
		systems.add(renderingSystem);
		systems.add(inputSystem);
		systems.add(physicsSystem);
		systems.add(meltSystem);
		systems.add(textboxCSystem);
		systems.add(gunSystem);

		for (CSystem s: systems) {
			for (Entity e: entityList) {
				s.addEntity(e);
			}
		}

		music.loop();
		hover.loop();
	}

	void loadLevel(String fileName) {
		tiledMap = new TmxMapLoader().load(fileName);

		Integer tileWidth = tiledMap.getProperties().get("tilewidth", Integer.class);

		createEntities(tileWidth, tiledMap.getLayers().get("aurora_and_waves").getObjects());
		createEntities(tileWidth, tiledMap.getLayers().get("unmovable").getObjects());
		createEntities(tileWidth, tiledMap.getLayers().get("spaceship").getObjects());
		createEntities(tileWidth, tiledMap.getLayers().get("aurora_and_waves").getObjects());
		createEntities(tileWidth, tiledMap.getLayers().get("ice_melting").getObjects());
		createEntities(tileWidth, tiledMap.getLayers().get("obj").getObjects());
		createEntities(tileWidth, tiledMap.getLayers().get("yeti_and_badger").getObjects());
		createEntities(tileWidth, tiledMap.getLayers().get("collision").getObjects());
		createEntities(tileWidth, tiledMap.getLayers().get("textbox").getObjects());


		MapProperties playerProperties = new MapProperties();
		playerProperties.put("pos.x", Float.parseFloat("15.6"));
		playerProperties.put("pos.y", Float.parseFloat("117"));
		playerProperties.put("render.height", Float.parseFloat("2.0"));
		playerProperties.put("render.width", Float.parseFloat("2.0"));
		playerProperties.put("phys.type", "circle");
		playerProperties.put("input", "yes");
		playerProperties.put("camera", "yes");
		playerProperties.put("render.texture", "level1/alien.png");
		playerProperties.put("light.type", "point");
		playerProperties.put("light.distance", 10f);
		playerProperties.put("light.color", "66666666");
		playerProperties.put("gun", true);
		playerProperties.put("gun.lightcolor.off", "FF8888FF");
		playerProperties.put("gun.lightcolor.on", "FF0000FF");



		Entity player = new Entity(playerProperties);
		entityList.add(player);
	}

	private void createEntities(Integer tileWidth, MapObjects objects) {
		for (MapObject obj : objects) {

			MapProperties p = obj.getProperties();
			Float x = p.get("x", Float.class);
			Float y = p.get("y", Float.class);
			Float w = p.get("width", Float.class);
			Float h = p.get("height", Float.class);
			x = x/tileWidth;
			y = y/tileWidth;
			w = w/tileWidth;
			h = h/tileWidth;

			x += w *0.5f;
			y += h *0.5f;

			p.put("render.height", h);
			p.put("render.width", w);
			p.put("pos.x", x);
			p.put("pos.y", y);
			Entity e = new Entity(p);
			e.center = utils.getCenterPosition(e);

			if (p.containsKey("textbox.hidden"))
			{
				e.isVisible = !(p.get("textbox.hidden", Boolean.class));
			}

			if (obj instanceof TiledMapTileMapObject)
			{
				TiledMapTileMapObject t = (TiledMapTileMapObject)obj;
				e.img = t.getTextureRegion().getTexture();
			}
			if (obj instanceof PolygonMapObject)
			{
				PolygonMapObject poly = (PolygonMapObject)obj;
				Polygon temp = poly.getPolygon();

				float[] vertices = temp.getVertices();
				float[] worldVertices = new float[vertices.length];

				for (int i = 0; i < vertices.length; ++i) {
					worldVertices[i] = vertices[i] / tileWidth;
				}

				Polygon polygon = new Polygon(worldVertices);
				e.polygon = polygon;

				p.put("phys.type", "polygon");
				p.put("phys.static", "yes");
			}

			entityList.add(e);
		}
	}


	@Override
	public void render () {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		for (CSystem s : systems) {
			s.update(game);
		}

		for (Entity e : game.toRemove) {
			for (CSystem s : systems) {
				s.removeEntity(e);
			}
		}

	}
	
	@Override
	public void dispose () {
		for (CSystem s: systems) {
			s.dispose();
		}
	}
}

