package no.gj.hg.systems;

import no.gj.hg.Entity;
import no.gj.hg.Game;

public interface CSystem {
    public void addEntity(Entity e);
    public void update(Game game);
    public void dispose();

    void removeEntity(Entity e);
}
