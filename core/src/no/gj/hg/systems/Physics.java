package no.gj.hg.systems;

import box2dLight.*;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import no.gj.hg.Entity;
import no.gj.hg.Game;

import java.util.ArrayList;
import java.util.List;

public class Physics implements CSystem {
    short stat = 1;
    short dyn = 2;

    World world;
    Box2DDebugRenderer debugRenderer;
    RayHandler rayHandler;
    Entity player;
    ConeLight playerGunLight;

    public Physics() {
        world = new World(new Vector2(0, -10), true);
        debugRenderer = new Box2DDebugRenderer();
        rayHandler = new RayHandler(world);

    }

    public void addEntity(Entity e) {
        if (e.props.containsKey("phys.type") && e.props.get("phys.type", String.class).equals("circle")) {
            addCircle(e);
        }
        if (e.props.containsKey("phys.type") && e.props.get("phys.type", String.class).equals("polygon")) {
            addPolygon(e);
        }
        if (e.props.containsKey("light.type")) {
            String type = e.props.get("light.type", String.class);
            if(type.equals("point")) {
                addPointLight(e);
            }
            else if(type.equals("cone"))
            {
                addConeLight(e);
            }
        }
        if(e.props.containsKey("gun"))
        {
            player = e;
            float distance = 15;
            Vector2 playerPos = utils.getCenterPosition(e);
            playerGunLight = new ConeLight(rayHandler, 100, new Color(0xFF8888FF), distance, playerPos.x, playerPos.y, -90, 30);
            playerGunLight.setXray(true);
            playerGunLight.attachToBody(e.body);
        }

        if(e.body != null) {
            e.body.setUserData(e);
        }
    }

    private void addConeLight(Entity e)
    {
        assert(e.props.containsKey("light.color"));
        assert(e.props.containsKey("light.distance"));
        assert(e.props.containsKey("light.direction_degree"));
        assert(e.props.containsKey("light.cone_degree"));

        Color color = getColor(e, "light.color");
        Float distance = e.props.get("light.distance", Float.class);
        Vector2 pos = utils.getCenterPosition(e);
        Float directionDegree = e.props.get("light.direction_degree", Float.class);
        Float coneDegree = e.props.get("light.cone_degree", Float.class);

        // rays per degree
        int rays = (int)Math.round(Math.ceil(coneDegree)) * 5;

        PositionalLight l = new ConeLight(rayHandler, rays, color, distance, pos.x, pos.y, directionDegree, coneDegree);
        if(e.body != null)
            l.attachToBody(e.body);
    }

    // rays per degree
    int RAYS_NUM = 360 * 5;
    private void addPointLight(Entity e) {
        assert(e.props.containsKey("light.color"));
        assert(e.props.containsKey("light.distance"));
        assert(e.props.containsKey("light.static_only"));

        Color color = getColor(e, "light.color");
        Float distance = e.props.get("light.distance", Float.class);
        Vector2 pos = utils.getCenterPosition(e);
        boolean isStaticOnly = e.props.containsKey("light.static_only") ? e.props.get("light.static_only", Boolean.class) : false;
        
        PositionalLight l = new PointLight(rayHandler, RAYS_NUM, color, distance, pos.x, pos.y);
        if(isStaticOnly) {
            Filter f = new Filter();
            f.maskBits = stat;
            l.setContactFilter(f);
        }

        if(e.body != null)
            l.attachToBody(e.body);
    }

    private Color getColor(Entity e, String key) {
        return new Color((int)Long.parseLong(e.props.get(key, String.class), 16));
    }

    private void addCircle(Entity e) {
        float x = e.props.get("pos.x", Float.class);
        float y = e.props.get("pos.y", Float.class);
        float r = e.props.get("render.height", Float.class)/2.0f;
        BodyDef bodyDef = new BodyDef();
        FixtureDef fixtureDef = new FixtureDef();
        if (!e.props.containsKey("phys.static")) {
            bodyDef.type = BodyDef.BodyType.DynamicBody;
            fixtureDef.filter.categoryBits = dyn;
        } else {
            bodyDef.type = BodyDef.BodyType.StaticBody;
            fixtureDef.filter.categoryBits = stat;
        }
        bodyDef.position.set(x, y);
        Body body = world.createBody(bodyDef);
        CircleShape circle = new CircleShape();
        circle.setRadius(r);

        fixtureDef.shape = circle;
        fixtureDef.density = 0.5f;
        fixtureDef.friction = 0.4f;
        fixtureDef.restitution = 0.6f;
        Fixture fixture = body.createFixture(fixtureDef);

        circle.dispose();
        e.body = body;
    }

    private void addPolygon(Entity e) {
        float x = e.props.get("pos.x", Float.class);
        float y = e.props.get("pos.y", Float.class);

        PolygonShape polygon = new PolygonShape();

        polygon.set(e.polygon.getVertices());

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.StaticBody;

        bodyDef.position.set(x, y);
        Body body = world.createBody(bodyDef);
        body.createFixture(polygon, 1);

        polygon.dispose();
        e.body = body;
    }

    Fixture closestFixture;
    float closestFraction;
    public void update(Game game) {
        rayHandler.setCombinedMatrix(game.camera);
        rayHandler.updateAndRender();
        //debugRenderer.render(world, game.camera.combined);

        shootGun(player);

        world.step(1/60f, 6, 2);
    }

    private void shootGun(Entity gunner) {
        // adjust gun light
        float deg = player.direction.angle();
        player.body.setTransform(player.body.getPosition(), (float)Math.toRadians(deg));

        // adjust gun light color
        Color color;
        float coneDeg;
        float distance;
        if(gunner.isGunActive)
        {
            coneDeg = 5;
            distance = 100;
            assert(gunner.props.containsKey("gun.lightcolor.on"));
            color = getColor(gunner, "gun.lightcolor.on");

            // find object to melt, and set melt on it
            Vector2 endpoint = new Vector2(player.body.getPosition());
            endpoint.add(player.direction.x * 100, player.direction.y * 100);
            closestFixture = null;
            closestFraction = 1.1f;
            world.rayCast(new RayCastCallback() {
                @Override
                public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
                    if(fraction < closestFraction)
                    {
                        closestFixture = fixture;
                        closestFraction = fraction;
                    }
                    return 1;
                }
            }, player.body.getPosition(), endpoint);

            if(closestFixture != null) {
                Entity e = (Entity) closestFixture.getBody().getUserData();
                if (e.props.containsKey("meltable")) {
                    e.props.put("melt", 1f);
                }
            }
        }
        else
        {
            coneDeg = 30;
            distance = 15;
            assert(gunner.props.containsKey("gun.lightcolor.off"));
            color = getColor(gunner, "gun.lightcolor.off");
        }
        playerGunLight.setColor(color);
        playerGunLight.setConeDegree(coneDeg);
        playerGunLight.setDistance(distance);
    }

    public void dispose() {
        rayHandler.dispose();
    }

    public void removeEntity(Entity e) {
        if (e.body != null) {
            world.destroyBody(e.body);
            e.body = null;
        }
    }
}
