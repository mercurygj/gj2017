package no.gj.hg.systems;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import no.gj.hg.Entity;
import no.gj.hg.Game;

import javax.xml.soap.Text;
import java.util.ArrayList;
import java.util.List;

public class RenderingCSystem implements CSystem {

    SpriteBatch batch = new SpriteBatch();;
    List<Entity> entities = new ArrayList<Entity>();


    public void addEntity(Entity entity) {
        if (entity.img != null) {
            entities.add(entity);
        }
        if (entity.img == null && entity.props.containsKey("render.texture")) {
            entity.img = new Texture(entity.props.get("render.texture", String.class));
            entities.add(entity);
        }
    }

    public void update(Game game) {

        batch.setProjectionMatrix(game.camera.combined);

        for (Entity e: entities) {

            if (e.isVisible) {

                batch.begin();
                // TODO: this is not optimized, hint: add a TextureRegion in entity
                Float width = e.props.get("render.width", Float.class);
                Float height = e.props.get("render.height", Float.class);

                if (e.body != null) {
                    batch.draw(e.img, e.body.getPosition().x - width / 2.0f, e.body.getPosition().y - height / 2.0f, width, height);
                } else if (e.center != null) {
                    batch.draw(e.img, e.center.x - width / 2.0f, e.center.y - height / 2.0f, width, height);
                }
                batch.end();
            }
        }
    }

    public void dispose() {
        batch.dispose();
        for (Entity e : entities) {
            e.img.dispose();
        }
    }

    public void removeEntity(Entity e) {
        entities.remove(e);
    }
}
