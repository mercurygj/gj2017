package no.gj.hg.systems;


import no.gj.hg.Entity;
import no.gj.hg.Game;

import java.util.ArrayList;
import java.util.List;

public class TextboxCSystem implements CSystem {

    List<Entity> textBoxes = new ArrayList<Entity>();
    Entity player = null;

    float activationRange = 0.0f;

    public void addEntity(Entity e) {
        if (e.props.containsKey("textbox.hidden"))
        {
            activationRange = e.props.get("textbox.range", Float.class);
            textBoxes.add(e);
        }
        if (e.props.containsKey("input"))
        {
            player = e;
        }
    }

    public void update(Game game) {

        for (Entity e : textBoxes) {
            if (e.props.get("textbox.hidden", Boolean.class)) {
                if (player.body.getPosition().dst(e.center) < activationRange) {
                    e.isVisible = true;
                } else {
                    e.isVisible = false;
                }
            }
        }
    }

    public void dispose() {

    }

    public void removeEntity(Entity e) {

    }
}
