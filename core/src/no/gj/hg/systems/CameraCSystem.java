package no.gj.hg.systems;

import no.gj.hg.Entity;
import no.gj.hg.Game;


public class CameraCSystem implements CSystem {

    Entity cameraSource;

    public void addEntity(Entity e) {
        if (e.props.containsKey("camera"))
        {
            cameraSource = e;
        }
    }

    public void update(Game game) {
        if (cameraSource != null) {
            game.camera.position.set(cameraSource.body.getPosition().x, cameraSource.body.getPosition().y, 0.0f);
            game.camera.update();
        }
    }


    public void dispose() {

    }

    public void removeEntity(Entity e) {
        if (e == cameraSource) cameraSource = null;
    }
}
