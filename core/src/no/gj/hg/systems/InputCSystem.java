package no.gj.hg.systems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.Vector2;
import no.gj.hg.Entity;
import no.gj.hg.Game;

public class InputCSystem implements CSystem {
    Entity entity;
    Sound hoverBooster = null;
    Sound meltSound = null;
    Sound gunSound = null;

    boolean isBoosting = false;

    boolean isGunActive = false;


    public InputCSystem(Sound hoverBooster, Sound meltSound, Sound gunSound)
    {
        this.hoverBooster = hoverBooster;
        this.meltSound = meltSound;
        this.gunSound = gunSound;
    }


    public void addEntity(Entity e) {
        if (e.props.containsKey("input")) {
            entity = e;
        }
    }

    public void update(Game game) {
        float forceUp = 35.0f;
        float forceRight = 15.0f;
        float forceDown = - 15.0f;

        boolean wasBoosting = isBoosting;

        isBoosting = false;
        if (Gdx.input.isKeyPressed(Input.Keys.ESCAPE)) {
            System.exit(0);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.W)) {
            entity.body.applyForceToCenter(0, forceUp, true);
            isBoosting = true;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            entity.body.applyForceToCenter(-forceRight, 0, true);
            isBoosting = true;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S)) {
            entity.body.applyForceToCenter(0, forceDown, true);
            isBoosting = true;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            entity.body.applyForceToCenter(forceRight, 0, true);
            isBoosting = true;
        }
        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)){

            if (!isGunActive) {
                entity.isGunActive = true;
                isGunActive = true;
                meltSound.loop();
                gunSound.loop();
            }
        }
        else
        {
            isGunActive = false;
            entity.isGunActive = false;
            meltSound.stop();
            gunSound.stop();
        }


        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            float y = 5;
            y++;
        }

        if (!wasBoosting && isBoosting)
        {
            hoverBooster.loop(0.5f);
        }
        if (!isBoosting)
        {
            hoverBooster.stop();
        }

        // calc gun direction
        Vector2 mousePos = new Vector2(Gdx.input.getX(), Gdx.input.getY());
        Vector2 screenMidPos = new Vector2(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);

        Vector2 dir = mousePos.sub(screenMidPos);
        entity.direction = dir;
        entity.direction.y = -entity.direction.y;
    }

    @Override
    public void dispose() {

    }

    @Override
    public void removeEntity(Entity e) {
        if (entity == e) entity = null;
    }
}
