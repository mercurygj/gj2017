package no.gj.hg.systems;

import no.gj.hg.Entity;
import no.gj.hg.Game;

import java.util.ArrayList;
import java.util.List;

public class MeltSystem implements CSystem {
    List<Entity> entities = new ArrayList<Entity>();

    public void addEntity(Entity e) {
        if (e.props.containsKey("meltable")) {
            entities.add(e);
        }
    }
    public void update(Game game) {
        for (Entity e: entities) {
            if (e.props.containsKey("melt")) {
                float d = e.props.get("melt", Float.class);
                float coldnessLeft = e.props.get("meltable", Float.class);
                coldnessLeft -= d;
                e.props.put("meltable", coldnessLeft);
                if (coldnessLeft <= 0) {
                    game.toRemove.add(e);
                } else {
                    e.props.remove("melt");
                }
            }
        }
    }
    public void dispose() {

    }

    public void removeEntity(Entity e) {
        entities.remove(e);
    }
}
