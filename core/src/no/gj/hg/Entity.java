package no.gj.hg;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

import java.util.Map;

public class Entity {
    public MapProperties props = null;

    Entity(MapProperties props) {
        this.props = props;
    }

    public Body body = null;
    public Texture img = null;
    public Vector2 center = null;
    public Polygon polygon = null;
    public boolean isVisible = true;
    public boolean isGunActive = false;
    public Vector2 direction = null;
}
