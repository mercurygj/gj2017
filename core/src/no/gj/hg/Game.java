package no.gj.hg;

import com.badlogic.gdx.graphics.OrthographicCamera;

import java.util.ArrayList;
import java.util.List;

public class Game {
    public OrthographicCamera camera;
    public List<Entity> toRemove = new ArrayList<Entity>();

    public Game() {
        camera = new OrthographicCamera(32, 18);
    }
}
